# TP-Base de la mise en réseau
## Networking 101: Organisation de votre infrastructure réseau
```bash
gcloud auth list
```
Cette commande affiche la liste des comptes Google Cloud authentifiés sur la machine locale, indiquant quel compte est actuellement actif pour les opérations gcloud.
```bash
gcloud config list project
```
Cette commande affiche la liste des comptes Google Cloud authentifiés sur la machine locale, indiquant quel compte est actuellement actif pour les opérations gcloud.


## Définir la région et la zone
Pour garantir un déploiement optimal de vos ressources dans des régions et zones géographiques appropriées, il est crucial de définir ces paramètres à l'avance. Ceci contribue à optimiser à la fois la latence et les coûts.
```bash
gcloud config set compute/zone "Zone"
export ZONE=$(gcloud config get compute/zone)

gcloud config set compute/region "Region"
export REGION=$(gcloud config get compute/region)
```

### Création d'un réseau VPC personnalisé
La première étape consiste à établir un réseau VPC personnalisé, qui servira de base à votre infrastructure cloud en facilitant la communication entre les différentes ressources que vous allez déployer.
```bash
gcloud compute networks create taw-custom-network --subnet-mode custom
```

### Ajout de sous-réseaux
Une fois le réseau VPC mis en place, vous pouvez créer des sous-réseaux dans différentes régions pour segmenter votre réseau en blocs plus petits, ce qui simplifie la gestion et l'application des politiques de sécurité.
```bash
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.0.0.0/16
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.1.0.0/16
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.2.0.0/16
```

### Configuration des règles de pare-feu
Pour sécuriser votre réseau et contrôler l'accès aux instances VM, il est nécessaire de définir des règles de pare-feu. Par exemple, la création d'une règle pour autoriser le trafic HTTP entrant.
```bash
gcloud compute firewall-rules create nw101-allow-http \
   --allow tcp:80 \
   --network taw-custom-network \
   --source-ranges 0.0.0.0/0 \
   --target-tags http
```

### Affichage de la liste des sous-réseaux
```bash
gcloud compute networks subnets list \
   --network taw-custom-network
```

### Déploiement d'instances VM
Enfin, vous pouvez déployer des instances VM dans votre réseau, en les plaçant dans les sous-réseaux appropriés. Ces instances serviront d'hôtes pour vos applications et services.
```bash
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http
```

### Ajout de règles de pare-feu supplémentaires
Pour une sécurité plus complète, vous pouvez ajouter des règles de pare-feu supplémentaires autorisant le trafic ICMP (ping), SSH et éventuellement RDP (pour l'accès à distance sous Windows).
```bash
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"
```

### Création d'instances VM supplémentaires
Pour tester la connectivité et la performance entre différentes zones et régions, il est recommandé de déployer des instances VM supplémentaires dans les sous-réseaux créés.
```bash
gcloud compute instances create us-test-01 \
--subnet subnet-Region \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
gcloud compute instances create us-test-02 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
gcloud compute instances create us-test-03 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```

### Tester la connectivité et la performance
Après la configuration du réseau et le déploiement des instances, il est crucial de tester la connectivité réseau et d'évaluer la performance. Cela peut inclure des tests de ping pour vérifier la connectivité et l'utilisation d'outils comme iperf pour mesurer la bande passante entre les instances.

```bash
# Sur l'instance source
ping -c 3 <adresse IP externe de l'instance cible>
```
```bash
# Installation d'iperf pour les tests de performance
sudo apt-get update && sudo apt-get install -y iperf
```
```bash
# Sur l'instance servant de serveur (écoute sur le port par défaut 5001)
iperf -s
```
```bash
# Sur l'instance cliente, pour initier le test
iperf -c <adresse IP interne de l'instance serveur> -t 30
```

# TP2 (TERRAFORM)

```bash
gcloud auth list
```
`gcloud auth list` affiche les comptes Google Cloud actuellement authentifiés sur la machine, permettant de voir les comptes utilisés pour les opérations avec gcloud.

## Résultat de la commande terraform
```bash
Usage: terraform [--version] [--help]  [args]

The available commands for execution are listed below. The most common, useful commands are shown first, followed by less common or more advanced commands. If you're just getting started with Terraform, stick with the common commands. For the other commands, please read the help and docs before usage.

Common commands: apply Builds or changes infrastructure console Interactive console for Terraform interpolations destroy Destroy Terraform-managed infrastructure env Workspace management fmt Rewrites config files to canonical format get Download and install modules for the configuration graph Create a visual graph of Terraform resources import Import existing infrastructure into Terraform init Initialize a Terraform working directory output Read an output from a state file plan Generate and show an execution plan providers Prints a tree of the providers used in the configuration push Upload this Terraform module to Atlas to run refresh Update local state file against real resources show Inspect Terraform state or plan taint Manually mark a resource for recreation untaint Manually unmark a resource as tainted validate Validates the Terraform files version Prints the Terraform version workspace Workspace management

All other commands: debug Debug output management (experimental) force-unlock Manually unlock the terraform state state Advanced state management 
```

```bash
gcloud config list project
```

La commande `gcloud config list project` montre le projet Google Cloud en cours de configuration, aidant à identifier rapidement le projet par défaut pour les opérations gcloud.

```bash
touch instance.tf
```

La commande `touch instance.tf` crée un fichier vide nommé "instance.tf" en bash. Ensuite, vous pouvez ouvrir l'éditeur de texte de Google et y ajouter le code ci-dessous dans le fichier "instance.tf".

```bash
resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-aefd70bc4ef6"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-east1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}
```
```bash
terraform init
```
## Terraform-init

`terraform init` prépare un nouveau répertoire de travail Terraform en configurant les plugins et modules essentiels pour gérer l'infrastructure décrite dans les fichiers de configuration Terraform.

Voici le résultat :

```bash
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/google...
- Installing hashicorp/google v5.15.0...
- Installed hashicorp/google v5.15.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
## Plan de Terraformation

```bash
terraform plan
```

La commande `terraform plan` génère un plan d'exécution Terraform en analysant les fichiers de configuration dans le répertoire de travail. Il affiche un résumé des actions prévues pour mettre en place l'infrastructure décrite, offrant ainsi une prévisualisation des modifications avant leur application.

Voici le résultat :
```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-aefd70bc4ef6"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-east1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }
              + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

google_compute_instance.terraform: Creating...
google_compute_instance.terraform: Still creating... [10s elapsed]
google_compute_instance.terraform: Creation complete after 18s [id=projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.

```
## Spectacle Terraform

```bash
terraform show
```

La commande `terraform show` présente l'état actuel de l'infrastructure gérée par Terraform, y compris les ressources créées et leurs attributs actuels. C'est un outil pratique pour vérifier l'état de l'infrastructure après les modifications.

Voici le résultat :
```bash
# google_compute_instance.terraform:
resource "google_compute_instance" "terraform" {
    can_ip_forward       = false
    cpu_platform         = "Intel Broadwell"
    current_status       = "RUNNING"
    deletion_protection  = false
    effective_labels     = {}
    enable_display       = false
    guest_accelerator    = []
    id                   = "projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    instance_id          = "3631703849039533754"
    label_fingerprint    = "42WmSpB8rSM="
    machine_type         = "e2-medium"
    metadata_fingerprint = "WRRcWNyHo0s="
    name                 = "terraform"
    project              = "qwiklabs-gcp-01-aefd70bc4ef6"
    self_link            = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/instances/terraform"
    tags_fingerprint     = "42WmSpB8rSM="
    terraform_labels     = {}
    zone                 = "us-east1-b"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"
        source      = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/zones/us-east1-b/disks/terraform"

        initialize_params {
            enable_confidential_compute = false
            image                       = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20240110"
            labels                      = {}
            provisioned_iops            = 0
            provisioned_throughput      = 0
            size                        = 10
            type                        = "pd-standard"
        }
    }

    network_interface {
        internal_ipv6_prefix_length = 0
        name                        = "nic0"
        network                     = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/global/networks/default"
        network_ip                  = "10.142.0.2"
        queue_count                 = 0
        stack_type                  = "IPV4_ONLY"
        subnetwork                  = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-aefd70bc4ef6/regions/us-east1/subnetworks/default"
        subnetwork_project          = "qwiklabs-gcp-01-aefd70bc4ef6"

        access_config {
            nat_ip       = "34.138.108.138"
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
        provisioning_model  = "STANDARD"
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }
}
```
